CREATE TABLE IF NOT EXISTS Manager(
    Id INT PRIMARY KEY AUTO_INCREMENT,
    Manager_name VARCHAR(20),
    Manager_location VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS Staff(
    Id INT PRIMARY KEY AUTO_INCREMENT,
    Staff_name VARCHAR(20),
    Staff_location VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS Staff_Manager(
    Id INT  PRIMARY KEY AUTO_INCREMENT,
    Staff_Id INT REFERENCES Staff(Id),
    Manager_Id INT REFERENCES Manager(Id) 
);

CREATE TABLE IF NOT EXISTS Client(
    Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(20),
    Location VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS Client_Staff_Manager(
    Id INT PRIMARY key AUTO_INCREMENT,
    Client_Id INT REFERENCES Client(Id),
    Staff_Manager_Id INT REFERENCES Staff_Manager(Id)
);

CREATE TABLE IF NOT EXISTS Client_Contract(
    Id INT PRIMARY KEY AUTO_INCREMENT,
    Client_Staff_Manager_Id INT REFERENCES Client_Staff_Manager(Id),
    Estimated_cost INT,
    Completion_date DATE 
);